from serial import Serial
import ubx
import logging

gpsport = Serial("/dev/ttyS4")
stream = ubx.UbxStream(gpsport)

while True:
    try:
        message = stream.read()
        print(message)
    except KeyboardInterrupt:
        break
    except Exception as e:
        logging.exception(e)