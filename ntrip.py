import requests
import threading
import logging
import time
from datetime import datetime

logger = logging.getLogger(__name__)

class NTRIP(threading.Thread):
    def __init__(self, host=None, mountpoint=None, device=None, port=2101, name=""):
        threading.Thread.__init__(self, daemon=True, name="NTRIP_CLIENT-"+name)
        self.host = host
        self.port = port
        self.mountpoint = mountpoint
        self.device = device
        self.lastData = None
        self.interruptFlag = threading.Event()
        self.stopFlag  = False
        self.enabled = bool(host and mountpoint)
        self.in_bytes = 0
        if name:
            self.logger=logger.getChild(name)
        else:
            self.logger = logger
    
    def run(self):
        while not self.stopFlag:
            try:
                self.interruptFlag.clear()
                if not self.enabled:
                    self.logger.info("Client disabled")
                    self.interruptFlag.wait()
                else:
                    self.worker()
            except KeyboardInterrupt:
                break
            except (ConnectionRefusedError, requests.exceptions.ConnectionError):
                self.logger.error("Connection to {host}:{port} refused, waiting and retrying".format(host=self.host, port=self.port))
                time.sleep(1)
            except requests.exceptions.ChunkedEncodingError:
                self.logger.error("Connection to {host}:{port} lost, retrying".format(host=self.host, port=self.port))
            except Exception as e:
                logger.exception(e)
                time.sleep(1)

    
    def worker(self):
        url = 'http://{host}:{port}/{mountpoint}'.format(
                host=self.host,
                port=self.port,
                mountpoint=self.mountpoint
            )
        
        headers = {
                "User-Agent": "NTRIP AA4CC/0.0",
                "Ntrip-Version": "Ntrip/2.0"
            }

        self.logger.info("Connecting to {}...".format(url))
        r = requests.get(url, stream=True, headers=headers)
        r.raise_for_status()

        self.logger.info("Connected")

        for c in r.iter_content(None):
            self.in_bytes += len(c)
            self.lastData = datetime.now()
            if self.device:
                self.logger.debug("Received {} bytes chunk, written to device".format(len(c)))
                self.device.write(c)
            else:
                self.logger.debug("Received {} bytes chunk".format(len(c)))

            if self.interruptFlag.is_set():
                break
        self.logger.info("Connection closed")
    
    def interrupt(self):
        self.interruptFlag.set()
        self.logger.debug("Thread is informed to interrupt, waiting")

    def stop(self, wait=None):
        self.stopFlag = True
        self.interrupt()
        self.join(wait)
        self.logger.debug("Thread is joined")
    
    def reload(self, host=None, port=None, mountpoint=None, disable=False):
        if host:
            self.host = host
        if port:
            self.port = port
        if mountpoint:
            self.mountpoint = mountpoint
        self.enabled = bool(self.host and self.mountpoint and not disable)
        self.interrupt()


if __name__ == "__main__":
    import click

    @click.group()
    def main():
        pass

    @main.command()
    @click.option('-h', '--host', default="localhost")
    @click.option('-p', '--port', default=2101)
    @click.option('-m', '--mount-point', default="AA4CC")
    def single(host, port, mount_point):
        logging.basicConfig(level=logging.DEBUG)
        n = NTRIP(host, mount_point, port=port)

        n.start()
        n.join()
    

    @main.command()
    @click.option('-h', '--host', default="localhost")
    @click.option('-p', '--port', default=2101)
    @click.option('-m', '--mount-point', default="AA4CC")
    @click.option('-c', '--count', default=15)
    def multiple(host, port, mount_point, count):
        logging.basicConfig(level=logging.DEBUG)
        clients = [NTRIP(host, mount_point, port=port, name="Client_{}".format(i)) for i in range(count)]
        for client in clients:
            client.start()
        for client in clients:
            client.join()
    
    main()
    