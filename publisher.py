import requests
import threading
import time
from datetime import datetime
import logging

logger = logging.getLogger(__name__)

class Publisher(threading.Thread):
    def __init__(self, datasource, server, path, period):
        threading.Thread.__init__(self, daemon=True, name="Publisher")
    
        self.datasource = datasource
        self.server = server
        self.path = path
        self.period = period

    def run(self):
        while True:
            try: 
                r = requests.post('http://{}/{}'.format(self.server, self.path), json=self.datasource())
                r.raise_for_status()
                logger.debug('Monitoring published')
                time.sleep(self.period)
            except requests.exceptions.ConnectionError:
                logger.error('Could not publish data, connection error')
                time.sleep(2)
            except Exception as e:
                logger.exception(e)
                time.sleep(2)

if __name__ == "__main__":
    def source():
        return {"a":6, 'b': [1,2,3], 'd': repr(datetime.now())}
    p = Publisher(source, "localhost:5200", "monitoring", 3)
    p.start()
    try:
        p.join()
    except KeyboardInterrupt:
        pass
        