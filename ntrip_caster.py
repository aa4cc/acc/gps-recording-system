#!/usr/bin/python3           # This is client.py file

import socket
import logging
import threading
from time import sleep
from queue import Queue, Full

logger = logging.getLogger(__name__)

class NTRIP_Caster(threading.Thread):
    def __init__(self, host=None, port=25698):
        threading.Thread.__init__(self, daemon=True, name=__name__)
        self.host = host
        self.port = port
        self.sent = 0
        self.socket = None
        self.queue = Queue(5)

    def run(self):
        while True:
            logger.info("Connecting to {}:{}...".format(self.host, self.port))
            if self.connect():
                while True:
                    b = self.queue.get()
                    try:
                        self.sent += len(b)
                        self.socket.sendall(b)
                        logger.debug("Sent chunk of {} bytes".format(len(b)))
                    except (BrokenPipeError, ConnectionResetError):
                        logger.error("Connection to broadcaster lost, reconecting")
                        break
                    except Exception as e:
                        logger.exception(e)
                        break
                
    def connect(self, host=None, port=None):
        if host:
            self.host = host
        if port:
            self.port = port

        self.close()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.socket.connect((self.host, self.port))
            logger.info("Connected")
        except ConnectionRefusedError as e:
            logger.error(e)
            sleep(2)
            return False
        except Exception as e:
            self.socket = None
            logger.exception(e)
        self.sent = 0
        return not self.socket is None

    @property
    def is_connected(self):
        return bool(self.socket)

    def write(self, b):
        if self.is_connected:
            try:
                self.queue.put(b, block=False)
            except Full:
                logger.warning("Caster queue is full")


    def close(self):
        if self.socket:
            self.socket.close()
        self.socket = None