#!/usr/bin/env python3

from flask import Flask, g, render_template, jsonify, Response, request
import time
import json
import pubsub
from queue import Queue

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

clients = []
last_instruction = ''
monitoring_status = {}


@app.route("/")
def index():
    return render_template("master.html")

def event_stream(also_monitoring=False):
    q = pubsub.subscribe('topic')
    yield 'data: {}\n\n'.format(last_instruction)
    if also_monitoring:
        yield 'event: monitoring\ndata: {}\n\n'.format(json.dumps(monitoring_status))
    while True:
        element = q.get()['data']
        if type(element) == str:
            yield 'data: {}\n\n'.format(element)
        elif also_monitoring:
            print('publishing monitoring')
            yield 'event: monitoring\ndata: {}\n\n'.format(json.dumps(element))

@app.route('/monitoring', methods=["POST"])
def monitoring_post():
    json = request.get_json()
    monitoring_status[json["id"]] = json

    pubsub.publish('topic', monitoring_status)
    return "OK"

@app.route('/instructions', methods=["POST"])
def publish():
    global last_instruction
    pubsub.publish('topic', request.form['text'])
    last_instruction = request.form['text']
    return "OK"

@app.route('/instructions')
def stream():
    monitoring_enable = request.args.get("monitoring", False)
    r =  Response(event_stream(monitoring_enable),
        mimetype="text/event-stream")
    r.headers["Access-Control-Allow-Origin"] = "*"
    return r

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5200, threaded=True)