# GPS recording system

Usage of this system is limited to Ublox 9 ZED-P

## Installation
All GPS receivers must be configured. Use the configuration from `10Hz_leader.txt` and `10Hz_other.txt` as reference. It is preferred to store configuration in flash, to be persistent.

### Server
1) Install Python3.5 or higher
1) Install and create a virtual environment (optional)
1) Clone this repository
1) Install required packages with `pip3 install -r requirements.txt`
1) Run (typically in screen or multiple consoles)
   - `python3 master.py` - Master interface (gets telemetry from all cars and transmits commands)
   - `python3 ntrip_broadcaster.py` - Server for distribution of RTCM corrections

### Cars
1) Install Python3.5 or higher
1) Install and create a virtual environment (optional)
1) Clone this repository
1) Install required packages with `pip3 install -r requirements.txt`
1) Edit settings.json
   1) Set `gps_port` to name of port created on your pc ex. `/dev/ttyUSB0`(Linux, WSL), `COM3` (Windows)
   1) Set `id` to name of your car, this value will be in all file logs
   1) Set `master` to address of master server
   1) Set `ntrip_server`, `ntrip_port`, `ntrip_mountpoint` to address of NTRIP  broadcaster
1) Run `python3 interface.py [-c path_to_settings.json] [-p different_gps_port]`
Option `-c` is optional, as default is used `settings.json`. Another example is settings_base.json, this is used for leader. Option `-p` overrides portname from config.

## Communication string of RTCM (in this version)

UBLOX --(USB)--> Leader PC --(Wifi)--> Cellphone --(LTE)--> Server --(LTE)--> Cellphone --(Wifi)--> Follower PC --(USB)--> UBLOX

## Known bugs
- NTRIP Server (caster) is sometimes unresponsive 
  - Restart helps
  - Need some investigation of reasons
- Leader (source of RCTM correction data) is slow if recording data
  - It may depend on the network connection between leader and server
  - The workaround is not to use the recording in the leader (used in December experiments)
- Data loss of RTCM messages (every 72-75 seconds)
  - Looks like some buffer overflow
  - Make better (simple) communication
  - Need more investigation

## Possible enhancements
- Make a better reliable connection between leader and followers
- Implement remote control of recording data
- Extend telemetry (if connection allows, realtime data)
- Replace students and laptops with Raspberry Pi (depends on the remote control and not using their LTE connection)
- Use units of Herman Elektronika for communication


## Alternative way

- Make working RTKlib for postprocessing of GPS data
- Use Ublox modules only for logging raw data
- The working configuration is not known till today (12.12.2019)