#!/usr/bin/env python3

from flask import Flask, g, render_template, jsonify, Response, request
import ublox
import serial
import time
import json
import string
import logging
import click
from datetime import datetime

from splitter import TcpSplitter
from logger import Logger
from ntrip import NTRIP
from ntrip_caster import NTRIP_Caster
from publisher import Publisher
from imu import Recorder

import fake

settings = {}

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

port = serial.Serial()
logger = Logger(port)
tcp_splitter = TcpSplitter(logger)
receiver = ublox.UbxReceiver(tcp_splitter)
ntrip = NTRIP(device=tcp_splitter)
ntrip_caster = NTRIP_Caster()
recorder = Recorder()


def data():
    hasNED = ublox.UbxNavRELPOSNED in receiver.messages
    hasPVT = ublox.UbxNavPVT in receiver.messages
    return {
            #'messages':[repr(m) for m in receiver.messages.values()],
            'position':{
                'latitude': round(receiver.messages[ublox.UbxNavPVT].lat, 7) if hasPVT else 0,
                'longitude': round(receiver.messages[ublox.UbxNavPVT].lon, 7) if hasPVT else 0,
                'accuracy': round(receiver.messages[ublox.UbxNavPVT].hAcc, 1) if hasPVT else 0,
                'groundSpeed': round(receiver.messages[ublox.UbxNavPVT].gSpeed, 2) if hasPVT else 0,
                'groundSpeedAcurracy': round(receiver.messages[ublox.UbxNavPVT].sAcc, 1) if hasPVT else 0,
                },
            'relative_position': {
                'N': receiver.messages[ublox.UbxNavRELPOSNED].relPosN if hasNED else 0,
                'E': receiver.messages[ublox.UbxNavRELPOSNED].relPosE if hasNED else 0,
                'D': receiver.messages[ublox.UbxNavRELPOSNED].relPosD if hasNED else 0,
                'Length': round(receiver.messages[ublox.UbxNavRELPOSNED].relPosLength, 2) if hasNED else 0,
                'Heading': round(receiver.messages[ublox.UbxNavRELPOSNED].relPosHeading, 3) if hasNED else 0,
                'LengthAcc': round(receiver.messages[ublox.UbxNavRELPOSNED].accLength, 2) if hasNED else 0,
                'HeadingAcc': round(receiver.messages[ublox.UbxNavRELPOSNED].accHeading, 2) if hasNED else 0,
                'Valid': receiver.messages[ublox.UbxNavRELPOSNED].relPosValid if hasNED else False
                },
            'time': str(receiver.messages[ublox.UbxNavPVT].datetime) if hasPVT else None,
            'u-center': tcp_splitter.hasClient,
            'logging': {
                'active': logger.isLogging,
                'filename': logger.file.name if logger.isLogging else "",
                'in_bytes': logger.in_bytes,
                'out_bytes': logger.out_bytes,
                'file_bytes': logger.file_bytes
            },
            'ntrip': {
                'host': ntrip.host,
                'mountpoint': ntrip.mountpoint,
                'enabled': ntrip.enabled,
                'in_bytes': ntrip.in_bytes
            },
            'ntrip_caster': {
                'active': ntrip_caster.is_connected,
                'sent': ntrip_caster.sent
            },
            'imu': {
                'file_size': recorder.file_size
            }
        }

def publisher_data():
    hasNED = ublox.UbxNavRELPOSNED in receiver.messages
    hasPVT = ublox.UbxNavPVT in receiver.messages
    return {
            'id': settings.get("id"),
            'position':{
                'latitude': round(receiver.messages[ublox.UbxNavPVT].lat, 7) if hasPVT else 0,
                'longitude': round(receiver.messages[ublox.UbxNavPVT].lon, 7) if hasPVT else 0,
                'accuracy': round(receiver.messages[ublox.UbxNavPVT].hAcc, 1) if hasPVT else 0,
                'groundSpeed': round(receiver.messages[ublox.UbxNavPVT].gSpeed, 1) if hasPVT else 0,
                'groundSpeedAcurracy': round(receiver.messages[ublox.UbxNavPVT].sAcc, 1) if hasPVT else 0,
                },
            'relative_position': {
                'Length': round(receiver.messages[ublox.UbxNavRELPOSNED].relPosLength, 2) if hasNED else 0,
                'LengthAcc': round(receiver.messages[ublox.UbxNavRELPOSNED].accLength, 2) if hasNED else 0,
                'Valid': receiver.messages[ublox.UbxNavRELPOSNED].relPosValid if hasNED else False
                },
            'time': str(receiver.messages[ublox.UbxNavPVT].datetime) if hasPVT else None,
            'logging': {
                'active': logger.isLogging,
                'filename': logger.file.name if logger.isLogging else "",
                'file_bytes': logger.file_bytes
            },
            'ntrip': {
                'enabled': ntrip.enabled,
                'in_bytes': ntrip.in_bytes
            },
        }

def event_stream():
    while True:
        yield 'data: {}\n\n'.format(json.dumps(data()))
        time.sleep(0.1)

@app.route('/', methods=['GET', 'POST'])
def homepage():
    return render_template('index.html', port=tcp_splitter.port, settings=settings)

@app.route('/stream')
def stream():
    return Response(event_stream(),
        mimetype="text/event-stream")

@app.route('/gps.json')
def gps():
    return jsonify(data())


@app.route('/log/open')
def open_log():
    filename = "{}_{:%Y%m%d_%H%M%S}".format(settings["id"], datetime.now())
    logger.open('static/data/'+filename+".ubx")
    recorder.open('static/data/'+filename+"_imu.txt")
    return "OK"

@app.route('/log/close')
def close_log():
    logger.close()
    recorder.close()
    return "OK"

@app.route('/make_point')
def make_point():
    info=request.args.get("info")
    gps_time = str(receiver.messages[ublox.UbxNavPVT].datetime) if ublox.UbxNavPVT in receiver.messages else None
    time=datetime.now()

    with open('static/data/{}_log.csv'.format(settings.get("id")), 'a+') as f:
        f.write('{}, {}, {}\n'.format(time, gps_time, info))

    return 'OK'

@app.route('/ntrip')
def ntrip_open():
    ntrip.reload(host=request.args["host"], mountpoint=request.args["mountpoint"])
    return "OK"

@click.command()
@click.option('-d', '--debug', is_flag=True)
@click.option('-u', '--update', is_flag=True)
@click.option('-c', '--config', type=click.File(), default="settings.json")
@click.option('-p', '--gps-port', type=str, default=None)
def main(debug=None, update=None, config=None, gps_port=None):
    global port
    logging.basicConfig(level=logging.DEBUG if debug else logging.INFO)

    if update:
        import os
        print("Updating from git....")
        os.system('git pull')

    settings.update(json.load(config))
    if gps_port is not None:
        settings['gps_port'] = gps_port
    logging.info("My ID is {}".format(settings.get('id')))
    logging.info("GPS port is {}".format(settings.get('gps_port')))


    if not settings["gps_port"].endswith(".ubx"):
        try:
            port.port = settings['gps_port']
            port.open()
        except serial.SerialException as e:
            logging.error(e)
            return
    else:
        port = fake.Port(open(settings['gps_port'], 'rb'))
        receiver.setParser(fake.GPS)
        logger.setDevice(port)
    tcp_splitter.port = settings.get('ucenter_port')

    ntrip.reload(
        host=settings.get('ntrip_server'),
        mountpoint=settings.get('ntrip_mountpoint', '')+"?"+settings.get('id', ""),
        port=str(settings.get('ntrip_port')),
    )

    if settings.get('ntrip_caster'):
        ntrip_caster.host = settings.get('ntrip_caster')
        receiver.unparsed_callback = ntrip_caster.write  
        ntrip_caster.start()   

    if settings.get('master'):
        publisher = Publisher(publisher_data, settings.get('master'), 'monitoring', 5)
        publisher.start()
    
    if settings.get('imu_port'):
        recorder.port = settings.get('imu_port')
        recorder.start()

    tcp_splitter.start()
    receiver.start()
    ntrip.start()
    app.run(port=settings.get('gui_port', 5000), threaded=True)

if __name__ == "__main__":
    main()
