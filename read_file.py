import ublox
import click
from collections import OrderedDict

format = [
        (ublox.UbxNavPVT, 'datetime', '{}'),
        (ublox.UbxNavPVT, 'lon', '{}'),
        (ublox.UbxNavPVT, 'lat', '{}'),
        (ublox.UbxNavPVT, 'height', '{}'),
        (ublox.UbxNavPVT, 'hMSL', '{}'),
        (ublox.UbxNavPVT, 'hAcc', '{}'),
        (ublox.UbxNavPVT, 'vAcc', '{}'),
        (ublox.UbxNavPVT, 'velN', '{}'),
        (ublox.UbxNavPVT, 'velE', '{}'),
        (ublox.UbxNavPVT, 'velD', '{}'),
        (ublox.UbxNavPVT, 'gSpeed', '{}'),
        (ublox.UbxNavPVT, 'headMot', '{}'),
        (ublox.UbxNavPVT, 'sAcc', '{}'),
        (ublox.UbxNavPVT, 'headAcc', '{}'),
        (ublox.UbxNavRELPOSNED, 'refStationId', '{}'),
        (ublox.UbxNavRELPOSNED, 'iTOW', '{}'),
        (ublox.UbxNavRELPOSNED, 'relPosN', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'relPosE', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'relPosD', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'relPosLength', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'relPosHeading', '{:.1f}'),
        (ublox.UbxNavRELPOSNED, 'accN', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'accE', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'accD', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'accLength', '{:.4f}'),
        (ublox.UbxNavRELPOSNED, 'accHeading', '{:.1f}'),
        (ublox.UbxNavRELPOSNED, 'gnssFixOK', '{}'),
        (ublox.UbxNavRELPOSNED, 'diffSoln', '{}'),
        (ublox.UbxNavRELPOSNED, 'relPosValid', '{}'),
        (ublox.UbxNavRELPOSNED, 'carrSoln', '{}'),
        (ublox.UbxNavRELPOSNED, 'isMoving', '{}'),
        (ublox.UbxNavRELPOSNED, 'refPosMiss', '{}'),
        (ublox.UbxNavRELPOSNED, 'refObsMiss', '{}'),
        (ublox.UbxNavRELPOSNED, 'relPosHeadingValid', '{}'),
        (ublox.UbxNavRELPOSNED, 'relPosNormalized', '{}')
    ]

def combine_messages(stream, types=()):
    msg_types = {}
    for message in stream:
        t = type(message)
        if t in types:
            msg_types[t] = message
            filtred = {k:v for k,v in msg_types.items() if  v.iTOW == message.iTOW}
            if len(filtred) == len(types):
                yield filtred


@click.group()
def main():
    pass

@main.command()
@click.argument('filename')
def dump(filename):
    with open(filename, 'rb') as file:
        stream = ublox.UbxStream(file)
        for pvt, relposned in combine_messages(stream, types=(ublox.UbxNavPVT, ublox.UbxNavRELPOSNED)):
            print(pvt, relposned)

@main.command()
@click.argument('filenames', nargs=-1)
@click.option('-w', '--write', is_flag=True)
@click.option('--pvt-only', is_flag=True)
def convert(filenames=None, write=False, pvt_only=False):
    for filename in filenames:
        file_in = filename
        file_out = ".".join(filename.split(".")[:-1])+".csv"

        print("Conversion...")
        print("Input:", file_in)
        if write:
            print("Output:", file_out)

        f = open(file_in, "rb")
        g = ublox.UbxStream(f)

        if write:
            f2 = open(file_out, "w")

        if pvt_only:
            variables = [x for x in format if x[0] == ublox.UbxNavPVT]
        else:
            variables = format

        header = "\t".join(x[1] for x in variables)
        format_str = "\t".join((x[2] for x in variables))
        messages = set(x[0] for x in variables)

        if write:
            f2.write("# "+header+"\n")
        else:
            print(header)

        for m in combine_messages(g, types=messages):
            values = [getattr(m[x[0]], x[1])for x in variables]
            line = format_str.format(*values)
            if write:
                f2.write(line+"\n")
            else:
                print(line)

if __name__ == "__main__":
    main()