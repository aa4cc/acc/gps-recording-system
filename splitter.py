import socket
import threading
import logging

logger = logging.getLogger(__name__)

class TcpSplitter(threading.Thread):
    def __init__(self, device, port=15621):
        threading.Thread.__init__(self, daemon=True)
        self.device = device
        self.port=port
        self.serversocket = None
        self.clientsocket = None
    
    def run(self):
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.bind(("0.0.0.0", port))
        self.serversocket.listen(5)
        while True:
            self.clientsocket = None
            print("Waiting for connection on port {}".format(self.port))
            try:
                self.clientsocket, addr = self.serversocket.accept()      

                print("Got a u-center connection from {}".format(addr))
            
                while True:
                    try:
                        data = self.clientsocket.recv(8192)
                        print('U-Center command')
                        self.device.write(data)
                    except (OSError, ConnectionResetError):
                        print('Connection lost')
                        break
                    except KeyboardInterrupt:
                        return
            except Exception as e:
                logger.exception(e)
    
    def start(self):
        if self.port:
            threading.Thread.start(self)
            logger.info("Starting splitter")
        else:
            logger.info("Splitter has no port, skipping")

    @property
    def hasClient(self):
        return self.clientsocket != None

    def write(self, b, *args, **kwargs):
        return self.device.write(b, *args, **kwargs)

    def read(self, *args, **kwargs):
        data = self.device.read(*args, **kwargs)
        if self.hasClient:
            try:
                self.clientsocket.sendall(data)
            except ConnectionResetError:
                print('Connection to U-center lost')
                self.clientsocket.close()
                self.clientsocket = None
            except Exception as e:
                logger.exception(e)
                self.clientsocket.close()
                self.clientsocket = None
        return data
