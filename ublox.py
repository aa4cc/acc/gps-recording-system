import struct
import logging
import threading
from datetime import datetime, date, time, timedelta

logger = logging.getLogger(__name__)

header_struct = "<BBH"

def ubx_checksum(packet):
    ck_a = 0
    ck_b = 0

    for b in packet:
        ck_a = (ck_a + b) & 0xFF
        ck_b = (ck_b + ck_a) & 0xFF

    return ck_a, ck_b

UBX_TYPE_MAP = {
    'U1': 'B',
    'RU1_3': 'B',
    'I1': 'b',
    'X1': 'B',
    'U2': 'H',
    'I2': 'h',
    'X2': 'H',
    'U4': 'L',
    'I4': 'l',
    'X4': 'L',
    'R4': 'f',
    'R8': 'd',
    'CH': 'c'
}

def read_until(obj, expected=b"\n", size=None):
    """\
    Read until an expected sequence is found ('\n' by default), the size
    is exceeded or until timeout occurs.
    """
    lenterm = len(expected)
    line = bytearray()
    while True:
        c = obj.read(1)
        if c:
            line += c
            if line[-lenterm:] == expected:
                break
            if size is not None and len(line) >= size:
                break
        else:
            break
    return bytes(line)

class UbxStream:
    def __init__(self, device=None, unparsed_callback=None):
        self.device = device
        self.unparsed_callback = unparsed_callback

    def __iter__(self):
        return self
    
    def __next__(self):
        return self.read()

    def read(self):
        while True:
            if hasattr(self.device, "read_until"):
                unparsed = self.device.read_until(b'\xb5\x62')[:-2]
            else:
                unparsed = read_until(self.device, b'\xb5\x62')[:-2]

            if unparsed:
                if self.unparsed_callback:
                    self.unparsed_callback(unparsed)
                    logger.debug("Sending unparsed of {} bytes".format(len(unparsed)))
                else:
                    logger.debug("Unparsed chunk of {} bytes".format(len(unparsed)))
            header = self.device.read(4)
            if len(header) == 0:
                raise StopIteration()
            if len(header) != 4:
                raise AssertionError("Header is is not valid {}".format(len(header)))
            ubx_cls, ubx_id, ubx_len = struct.unpack(header_struct, header)
            payload = self.device.read(ubx_len)
            ubx_crc = tuple(self.device.read(2))
            packet_crc = ubx_checksum(header+payload)
            
            if ubx_crc != packet_crc:
                continue

            return UbxMessage.from_binary(ubx_cls, ubx_id, payload)

    def filter(self, types=()):
        if not hasattr(types, "__iter__"):
            types = (types,)
        return filter(lambda x: any([issubclass(type(x), t) for t in types]), self)

class UbxReceiver(threading.Thread):
    def __init__(self, device=None, unparsed_callback=None):
        threading.Thread.__init__(self, daemon=True)
        self.stream = None
        self.setParser(UbxStream, device, unparsed_callback)
        self.device = device
        self.messages = {}

    def setParser(self, parser, device=None, unparsed_callback=None):
        if device is None:
            device = self.device
        if unparsed_callback is None:
            unparsed_callback = self.unparsed_callback
        self.stream = parser(device=device, unparsed_callback=unparsed_callback)

    @property
    def unparsed_callback(self):
        if self.stream:
            return self.stream.unparsed_callback
        else:
            return None
    
    @unparsed_callback.setter
    def unparsed_callback(self, value):
        self.stream.unparsed_callback = value

    @property
    def device(self):
        if self.stream:
            return self.stream.device
        else: 
            return None

    @device.setter
    def device(self, value):
        self.stream.device = value

    def run(self):
        while True:
            try:
                message = self.stream.read()
                self.messages[message.__class__] = message
            except KeyboardInterrupt:
                break
            except StopIteration:
                logger.info("File read end")
                break
            except Exception as e:
                logging.exception(e)

class UbxMessage:
    ubx_cls = None
    ubx_id = None
    parameter_list = {}

    def __init__(self, ubx_cls, ubx_id, payload):
        self.ubx_cls = ubx_cls
        self.ubx_id = ubx_id
        self.payload = payload

    @classmethod
    def from_binary(cls, ubx_cls, ubx_id, payload):
        for ubx_class_type in cls.__subclasses__():
            if ubx_class_type.ubx_cls == ubx_cls:
                for ubx_id_type in ubx_class_type.__subclasses__():
                    if ubx_id_type.ubx_id == ubx_id:
                        obj = ubx_id_type(payload)
                        return obj
                return ubx_class_type(ubx_cls, ubx_id, payload)
        return cls(ubx_cls, ubx_id, payload)

    def unpack(self, packet=None):
        unpack_format = "<"+"".join((UBX_TYPE_MAP[x[1]] if x[0] != 'reserved' else x[1] for x in self.parameter_list))
        unpack_size = struct.calcsize(unpack_format)

        if packet is None:
            packet = bytes(unpack_size)

        if unpack_size != len(packet):
            raise ValueError('Cannot import message {} with len(payload) = {}, required len is {}'.format(self.__class__.__name__, len(packet), unpack_size))

        data = struct.unpack(unpack_format, packet)

        i=0
        for par in self.parameter_list:
            if par[0] != 'reserved':
                name = par[0]
                value = data[i]

                if (len(par) >= 3) and (par[1][0] in 'UI') and (type(par[2]) is float):
                    value = value * par[2]
                    pass
                elif (len(par) >= 3) and (par[1][0] in 'UI') and type((par[2]) is tuple):
                    value = par[2][value]

                elif (len(par) >= 3) and (par[1][0] in 'X'):
                    for index, bname in enumerate(par[2]):
                        setattr(self, bname, bool(value & (1<<index)))

                setattr(self, name, value)
                i+=1

    def __repr__(self):
        return "<ublox.{}(cls=0x{:02x}, id=0x{:02x}, len(payload)={})>".format(self.__class__.__name__, self.ubx_cls, self.ubx_id, len(self.payload))

class UbxNavClass(UbxMessage):
    ubx_cls=0x01

class UbxNavRELPOSNED(UbxNavClass):
    ubx_id=0x3c

    parameter_list = [
        ('version', 'U1'), #1
        ('reserved', 'x'), #2
        ('refStationId', 'U2'), #4
        ('iTOW', 'U4'), #8
        ('relPosN', 'I4', 1e-2), #12
        ('relPosE', 'I4', 1e-2), #16
        ('relPosD', 'I4', 1e-2), #20
        ('relPosLength', 'I4', 1e-2), #24
        ('relPosHeading', 'I4', 1e-5), #28
        ('reserved', '4x'), #32
        ('relPosHPN', 'I1', 1e-4), #33
        ('relPosHPE', 'I1', 1e-4), #34
        ('relPosHPD', 'I1', 1e-4), #35
        ('relPosHPLength', 'I1', 1e-4), #36
        ('accN', 'U4', 1e-4), #40
        ('accE', 'U4', 1e-4), #44
        ('accD', 'U4', 1e-4), #48
        ('accLength', 'U4', 1e-4), #52
        ('accHeading', 'U4', 1e-5), #56
        ('reserved', '4x'), #60
        ('flags', 'X4', ('gnssFixOK', 'diffSoln', 'relPosValid', 'carrSoln', 'isMoving', 'refPosMiss', 'refObsMiss', 'relPosHeadingValid', 'relPosNormalized')) #64
    ]

    def __init__(self, payload=None):
        self.unpack(payload)
        self.relPosN += self.relPosHPN
        self.relPosE += self.relPosHPE
        self.relPosD += self.relPosHPD
        self.relPosLength += self.relPosHPLength

    def __repr__(self):
        return "<ublox.UbxNavRELPOSNED(N={0.relPosN:.3f}, E=N={0.relPosE:.3f}, D={0.relPosD:.3f}, Length={0.relPosLength:.3f}, Heading={0.relPosHeading:.1f}>".format(self)
    

class UbxNavPVT(UbxNavClass):
    ubx_id=0x07

    parameter_list = [
        ('iTOW', 'U4'),
        ('year', 'U2'),
        ('month', 'U1'),
        ('day', 'U1'),
        ('hour', 'U1'),
        ('min', 'U1'),
        ('sec', 'U1'),
        ('valid', 'X1', ('validDate', 'validTime', 'fullyResolved', 'validMag')),
        ('tAcc', 'U4'),
        ('nano', 'I4'),
        ('fixType', 'U1', ('NoFix', 'DeadRecogningOnly', '2D-fix', '3D-fix', 'GNSS+Deadreckogning', 'TimeOnly-fix')),
        ('flags', 'X1'),
        ('flags2', 'X1'),
        ('numSV', 'U1'),
        ('lon', 'I4', 1e-7),
        ('lat', 'I4', 1e-7),
        ('height', 'I4', 1e-3),
        ('hMSL', 'I4', 1e-3),
        ('hAcc', 'U4', 1e-3),
        ('vAcc', 'U4', 1e-3),
        ('velN', 'I4', 1e-3), 
        ('velE', 'I4', 1e-3), 
        ('velD', 'I4', 1e-3),
        ('gSpeed', 'I4', 1e-3),
        ('headMot', 'I4', 1e-5),
        ('sAcc', 'U4', 1e-3),
        ('headAcc', 'U4', 1e-5),
        ('pDOP', 'U2', 1e-2),
        ('flags3', 'X1'),
        ('reserved', '5x'),
        ('headVeh', 'I4', 1e-5),
        ('magDec', 'I2', 1e-2),
        ('magAcc', 'U2', 1e-2)
    ]

    def __init__(self, payload=None):
        self.unpack(payload)
    
    @property
    def datetime(self):
        try:
            return datetime(self.year, self.month, self.day, self.hour, self.min, self.sec) + timedelta(0,0,self.nano/1000)
        except ValueError:
            return datetime(1900, 1, 1)

    @property
    def latlon(self):
        return self.lat, self.lon
    
    def __repr__(self):
        return "<ublox.UbxNavPVT({0.datetime}, ({0.latlon[0]:.7f}, {0.latlon[1]:.7f}))>".format(self)

class UbxNavHPPOSLLH(UbxNavClass):
    ubx_id=0x14

    parameter_list = [
        ('version', 'U1'),
        ('reserved', '2x'),
        ('flags', 'X1', ('InvalidLlh')),
        ('iTOW', 'U4'),
        ('lon', 'I4', 1e-7),
        ('lat', 'I4', 1e-7),
        ('height', 'I4', 1e-3),
        ('hMSL', 'I4', 1e-3),
        ('lonHp', 'I1', 1e-9),
        ('latHp', 'I1', 1e-9),
        ('heightHp', 'I1', 1e-4),
        ('hMSLHp', 'I1', 1e-4),
        ('hAcc', 'U4', 1e-4),
        ('vAcc', 'U4', 1e-4)
    ]

    def __init__(self, payload=None):
        self.unpack(payload)
        self.lonHp += self.lon
        self.latHp += self.lat
        self.heightHp += self.height
        self.hMSLHp += self.hMSL


    @property
    def latlon(self):
        return self.lat, self.lon
    
    def __repr__(self):
        return "<ublox.UbxNavHPPOSLLH({0.latHp}, {0.lonHp}, {0.hMSLHp})>".format(self)

class UbxNavTIMEUTC(UbxNavClass):
    ubx_id = 0x21

    parameter_list = [
        ('iTOW', 'U4'),
        ('tAcc', 'U4', 1e-9),
        ('nano', 'I4'),
        ('year', 'U2'),
        ('month', 'U1'),
        ('day', 'U1'),
        ('hour', 'U1'),
        ('min', 'U1'),
        ('sec', 'U1'),
        ('valid', 'X1', ('validDate', 'validTime', 'fullyResolved', 'validMag')),
    ]

    def __init__(self, packet=None):
        self.unpack(packet)

    @property
    def datetime(self):
        try:
            return datetime(self.year, self.month, self.day, self.hour, self.min, self.sec) + timedelta(0,0,self.nano/1000)
        except ValueError:
            return datetime(1900, 1, 1)

    def __repr__(self):
        return "<ublox.UbxNavTIMEUTC({0.datetime})>".format(self)

class UbxNavSIG(UbxNavClass):
    ubx_id = 0x43

    def __init__(self, packet):
        self.payload = packet

def main():
    import serial
    import sys

    for message in UbxStream(serial.Serial(sys.argv[1])):
        print(message)

if __name__ == "__main__":
    main()