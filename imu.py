import serial
import threading
import time
import logging

logger = logging.getLogger(__name__)

class Recorder(threading.Thread):
    def __init__(self, port=None):
        threading.Thread.__init__(self, daemon=True)
        self.port = port
        self.filename = None
        self.irq_flag = threading.Event()
        self.file_size = 0

    def run(self):
        try:
            with serial.Serial(self.port, timeout=1) as port:
                while True:
                    logger.info("IMU logger ready on port {}".format(self.port))
                    self.irq_flag.wait()
                    self.irq_flag.clear()
                    if self.filename:
                        logger.info("Opening file {}".format(self.filename))
                        with open(self.filename, "ab") as file:
                            logger.debug("Clearing data in buffer")
                            n = 0
                            while port.in_waiting:
                                n += len(port.read(port.in_waiting))
                            logger.debug("Buffer cleared ({} bytes)".format(n))
                            self.file_size = 0
                            for line in port:
                                self.file_size += len(line)
                                file.write(line)
                                file.flush()
                                if self.irq_flag.is_set():
                                    self.irq_flag.clear()
                                    break
                        self.file_size = 0
                        logger.info("File closed")
        except serial.SerialException as e:
            logger.error(e)


    def open(self, filename):
        self.filename = filename
        self.irq_flag.set()

    def close(self):
        self.filename = None
        self.irq_flag.set()

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    r = Recorder("/dev/ttyS18")
    r.start()