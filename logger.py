import logging

class Logger():
    def __init__(self, device):
        self.file = None
        self.device = device
        self.in_bytes = 0
        self.out_bytes = 0
        self.file_bytes = 0
        self.logger = logging.getLogger(__name__)

    def setDevice(self, device):
        self.logger.warning("Changing device! to {}".format(device))
        self.device = device

    @property
    def isLogging(self):
        return self.file is not None

    def open(self, file):
        self.logger.info("Openning file {}".format(file))
        self.file = open(file, 'wb')
        self.file_bytes = 0

    def close(self):
        self.logger.info("Closing file")

        if self.file:
            self.file.close()
        self.file = None
        self.file_bytes = 0

    def write(self, b, *args, **kwargs):
        self.out_bytes += len(b)
        return self.device.write(b, *args, **kwargs)

    def read(self, *args, **kwargs):
        data = self.device.read(*args, **kwargs)
        self.in_bytes += len(data)
        if self.isLogging:
            self.file_bytes += len(data)
            self.file.write(data)
            self.file.flush()
        return data