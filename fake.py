from time import monotonic, sleep
import ublox
import logging

logger = logging.getLogger(__name__)

class Port():
    def __init__(self, file):
        self.file = file

    def read(self, *args, **kwargs):
        return self.file.read(*args, **kwargs)

    def write(self, b):
        logger.debug('Writing {} bytes into fake GPS'.format(len(b)))


class GPS(ublox.UbxStream):
    def __init__(self, *args, scale=1.0, **kwargs):
        ublox.UbxStream.__init__(self, *args, **kwargs)
        self.lastMessageDatetime = None
        self.lastMessageMonotonic = None

    def read(self):
        m = ublox.UbxStream.read(self)
        if hasattr(m, "datetime"):
            if self.lastMessageDatetime:
                diff_a = (m.datetime - self.lastMessageDatetime).total_seconds()
                diff_b = monotonic() -self.lastMessageMonotonic
                sleep_time = diff_a-diff_b
                if sleep_time > 0:
                    #print(sleep_time)
                    sleep(sleep_time)
            self.lastMessageDatetime = m.datetime
            self.lastMessageMonotonic = monotonic()
        return m

if __name__ == "__main__":
    import click
    @click.group()
    def main():
        pass

    @main.command()
    @click.argument('file', type=click.File("rb"))
    def gps(file):
        port = Port(file)
        for m in GPS(port):
            print(m)
    main()