#!/usr/bin/env python3

import time
import logging
import threading
import socket
import pubsub
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn


logger = logging.getLogger(__name__)

class Streamer(threading.Thread):
    def __init__(self, port=2100):
        threading.Thread.__init__(self, daemon=True)
        self.port=port
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serversocket.bind(("0.0.0.0", port))
        self.clientsocket = None
        self.length = 0
    
    def run(self):
        self.serversocket.listen(5)
        while True:
            self.clientsocket = None
            print("Waiting for source connection on port {}".format(self.port))
            try:
                self.clientsocket, addr = self.serversocket.accept()      

                print("Got a source connection from {}".format(addr))
            
                while True:
                    try:
                        data = self.clientsocket.recv(8192)
                        self.length += len(data)
                        if data:
                            pubsub.publish("data", data)
                        else:
                            self.clientsocket.close()
                            break
                        print("Streaming to /AA4CC endpoint {} kb\r".format(round(self.length/1000)), end="", flush=True)
                    except (OSError, ConnectionResetError):
                        print('Connection lost')
                        break
                    except KeyboardInterrupt:
                        return
            except Exception as e:
                logging.exception(e)

Streamer(25698).start()

class NTRIP_Source():
    def __init__(
        self,
        name,
        identifier,
        format,
        format_details,
        carrier,
        navigation_system,
        network,
        country,
        latitude,
        longitude,
        nmea,
        solution,
        compression,
        fee,
        bitrate
    ):
        self.name = name
        self.identifier = identifier
        self.format = format
        self.format_details = format_details
        self.carrier = carrier
        self.navigation_system = navigation_system
        self.network = network
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
        self.nmea = nmea
        self.solution = solution
        self.compression = compression
        self.fee = fee
        self.bitrate = bitrate

    def sourcetable_line(self):
        return "STR;{0.name};{0.identifier};{0.format};{0.format_details};{0.carrier};{0.navigation_system};{0.network};{0.country};{0.latitude};{0.longitude};{0.nmea};{0.solution};U_BLOX;{0.compression};{0.fee};N;{0.bitrate};none".format(self).encode()

sources = [
    NTRIP_Source(
        name="AA4CC",
        identifier="Ublox Base",
        format="RTCM 3.3",
        format_details="1005(0),1074(0),1077(0),1084(0),1087(0),1094(0),1097(0),1124(0),1127(0),1230(0),4072.0(0),4072.1(0)",
        carrier=2,
        navigation_system="GPS+GLO+GAL+BDS",
        network=None,
        country="CZ",
        latitude=50.076797,
        longitude=14.417953,
        nmea=0,
        solution=0,
        compression=None,
        fee=False,
        bitrate=5000
    )
]

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

class Ntrip_Stream(BaseHTTPRequestHandler):
    def do_HEAD(self):
        path = self.path.split("?",1)[0]
        if path == "/":
            self.sourcetable()
            return True
        if path.upper() != "/AA4CC":
            self.send_error(404)
            return
        self.protocol_version = "HTTP/1.1"
        self.send_response(200)
        self.send_header('Ntrip-Version', "Ntrip/2.0")
        self.send_header('Cache-Control', " no-store,no-cache,max-age=0")
        self.send_header('Pragma', "no-cache")
        self.send_header('Connection', "close")
        self.send_header('Content-Type', " gnss/data")
        self.send_header('Transfer-Encoding', " chunked")
        self.end_headers()

    def do_GET(self):
        if self.do_HEAD():
            return
        queue = pubsub.subscribe("data")
        try:
            while True:
                chunk = queue.get()['data']
                self.send_chunk(chunk)
            self.termination_chunk()
        except (BrokenPipeError, ConnectionResetError):
            logger.info("Client lost")
        except Exception as e:
            logger.exception(e)

    def sourcetable(self):
        buffer = bytearray()
        for source in sources:
            buffer += source.sourcetable_line()+b'\n'
        buffer+= b"ENDSOURCETABLE\r\n"

        self.protocol_version = "HTTP/1.1"
        self.send_response(200)
        self.send_header('Ntrip-Version', "Ntrip/2.0")
        self.send_header('Ntrip-Flags', "st_filter,st_auth,st_match,st_strict,rtsp,plain_rtp")
        self.send_header('Content-Type', "gnss/sourcetable")
        self.send_header('Content-Length', str(len(buffer)))
        self.send_header('Connection', "close")
        self.end_headers()
        self.wfile.write(buffer)



    def version_string(self):
        return "AA4CC Caster/0.0"

    def send_chunk(self, data):
        self.wfile.write('{:x}\r\n'.format(len(data)).encode())
        self.wfile.write(data+b'\r\n')
        self.wfile.flush()
    
    def termination_chunk(self):
        self.send_chunk(b'')


if __name__ == '__main__':
    port = 2101
    logging.basicConfig(level=logging.DEBUG)
    httpd = ThreadedHTTPServer(('0.0.0.0', port), Ntrip_Stream)
    print(time.asctime(), "Server Starts - port {}".format(port))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        print("Closing server, hold on")
    httpd.server_close()
    print(time.asctime(), "Server Stops - port {}".format(port))